# MOAT bot

## Quickstart

* use `Python3.7.4`

```shell script
git clone git@gitlab.com:CIRSFID/moatbot.git
cd moatbot
python3.7 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

* put a `.env` file in `moatbot/moatbot` (where the `settings.py` file is located):
```.env
# DEV SETTINGS
export DEV_MOAT_RPC_URL=http://127.0.0.1:8545  # ganache
export DEV_MOAT_SECRET_KEY=<A DJANGO SECRET KEY>
export DEV_MOAT_PRIVATE_KEY=<MASTER ACCOUNT PRIVATE KEY>
export DEV_MOATBOT_API_TOKEN=<DEV TELEGRAM API TOKEN>
export DEV_MOAT_CONTRACT_ADDRESS=0x46DEF...
export DEV_MOAT_ABI_PATH=/path/to/abi/MOATcoin.dev.json
export DEV_MOAT_LOG_PATH=/path/to/log/moatbot.dev.log

# PRODUCTION SETTINGS
export MOAT_RPC_URL=<YOUR RPC URL>
export MOAT_SECRET_KEY=<A DJANGO SECRET KEY>
export MOAT_PRIVATE_KEY=<MASTER ACCOUNT PRIVATE KEY>
export MOATBOT_API_TOKEN=<PROD API TOKEN>
export MOAT_CONTRACT_ADDRESS=0x92a...
export MOAT_ABI_PATH =/path/to/abi/MOATcoin.prod.json
export MOAT_LOG_PATH=/path/to/log/moatbot.prod.log
export MOAT_PG_DB=moatbot
export MOAT_PG_USER=moat
export MOAT_PG_PWD=<DB_PWD>
export MOAT_PG_HOST=localhost
export MOAT_PG_PORT=5432

```

* to init the database, run
```shell script
python manage.py migrate
```

* create a superuser to manage the db through Django's built-in control panel:
```shell script
python manage.py createsuperuser
```

* to setup the PostgresQL db for production, in the postgres shell run:
```shell script
CREATE DATABASE moatbot;
CREATE USER moat WITH PASSWORD 'YourPassword';
ALTER ROLE moat SET client_encoding TO 'utf8';
ALTER ROLE moat SET default_transaction_isolation TO 'read committed';
ALTER ROLE moat SET timezone TO 'Europe/Rome';
GRANT ALL PRIVILEGES ON DATABASE moatbot TO moat;
```

* to run the server to access the admin panel:
```shell script
python manage.py runserver
```

* edit [candidate.json](bot/patterns/candidate.json) accordingly to your userbase;

* to run the bot:
```shell script
python telegram_bot.py
```

* to run in production, set `DEBUG = False` in `moatbot/settings.py`

## migrate legacy contract data
 - be sure to have the data in the root directory in `contract_data.json`, as follows:
```json
{
  "candidates": [
    {
      "candidate_key": "moat",
      "telegram_id": 0,
      "address": "0x...",
      "private_key": "0x..",
      "moat_balance": 10000000000000000000000,
      "stakes": []
    },
    {
      "candidate_key": "biagio",
      "telegram_id": 705420310,
      "address": "0x...",
      "private_key": "0x..",
      "moat_balance": 31000000000000000000,
      "stakes": [
        "0x...",
        "0x..."
      ]
    }
  ],
  "challenges": {
    "nadia": [
      "0x..."
    ],
    "jacopo_m": [
      "0x...",
      "0x...",
      "0x..."
    ]
  }
}
```

* run the import script exactly once:
```shell script
python contract_migrate.py
```
