import os

import django

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'moatbot.settings')
django.setup()

from bot.telegram_bot import dp, executor
from moatbot.settings import DEBUG

if __name__ == '__main__':
    if not DEBUG:
        print("!! PRODUCTION !!")
    executor.start_polling(dp, skip_updates=True)
